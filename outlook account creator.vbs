'*************************************'
' Outlook Account Creator Josef Nosov '
'*************************************'
on error resume next									' If errors occur, ignore (try catch)
Dim shell, fso, source, target							' initializes the objects.
Set shell = Wscript.CreateObject("wscript.Shell")		' Allows the application to run
Set fso = CreateObject("Scripting.FileSystemObject")	' The file system object stream
source = left(wscript.ScriptFullName, (len(wscript.ScriptFullName))-(len(wscript.ScriptName))) & "outlook.prf"	
					' Finds the location of the folder, outlook.prf should be in the same folder.
target = shell.ExpandEnvironmentStrings("%TEMP%") & "\outlookprf\"		' The target folder, will be placed in %TEMP%\outlookprf\
If (Not fso.FolderExists(target)) Then		' Creates a folder called outlookprf in %TEMP% if does not exist
    fso.CreateFolder target					' Creates the folder
End If
If (fso.FileExists(source)) Then			' Checks to see if file exists in this source folder.
    fso.CopyFile source, target, True		' Copies the file from the source to the target.
End if

createInputBox "Enter your name", "Enter your name", "Your Name Here", "ReplaceDisplayName"
createInputBox "Enter your net id", "Enter your net id", "Your NetID Here", "ReplaceNetID"

shell.Run  "outlook.exe /resetnavpane /importprf " & chr(34) & target & "outlook.prf" & chr(34), 1, true 	
			' imports the prf into the location, stalls until application is closed before deleting temp prf.
			' Runs the following: outlook.exe /resetnavpane /importprf "%TEMP%\outlookprf\outlook.prf"
fso.DeleteFolder Left(target, Len(target) - 1)					'  Once outlook is closed, deleted the folder and file.



Function createInputBox(title, body, input, replacename)
	Dim getFile, readFile
	Set getFile = fso.OpenTextFile(target & "outlook.prf", 1)	' opens and reads the prf in temp (1 for read)
	readFile = getFile.ReadAll									' reads the file from temp and stores into readFile
	getFile.Close												' closes the stream
	Set getFile = fso.OpenTextFile(target & "outlook.prf", 2)	' opens and writes to the prf (2 for write)
	getFile.WriteLine Replace(readFile, replacename, inputbox(title,body,input))	' writes to prf the input placed into inputbox
	getFile.Close												' closes write stream.
End Function